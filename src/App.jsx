import { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header/Header";
import Board from "./components/Board/Board";
import { Home } from "./components/Home/Home";
import { Spinner, Text, Flex } from "@chakra-ui/react";

function App() {
  const [userData, setUserData] = useState([]);
  const [isError, setIsError] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    axios
      .get(
        "https://api.trello.com/1/members/me/boards?key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8"
      )
      .then((res) => {
        setUserData(res.data);
        setIsLoaded(true);
      })
      .catch((err) => setIsError(true));
  }, []);

  if (isLoaded && !isError) {
    return (
      <>
        <Header />
        <Routes>
          <Route
            path="/"
            element={<Home userData={userData} setUserData={setUserData} />}
          ></Route>
          <Route path="/boards/:id" element={<Board />} />
        </Routes>
      </>
    );
  } else {
    return (
      <>
        {isError ? (
          <Text>Error</Text>
        ) : (
          <Flex justifyContent={"center"} mt={"20rem"}>
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="pink"
              size="xl"
            />
          </Flex>
        )}
      </>
    );
  }
}

export default App;
