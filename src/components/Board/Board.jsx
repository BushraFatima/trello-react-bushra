import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Text,
  List,
  ListItem,
  Spinner,
} from "@chakra-ui/react";
import axios from "axios";
import { useParams } from "react-router-dom";
import ListCards from "../List/ListCards";
import "./Board.css";

function Board() {
  const { id } = useParams();
  const [creatingList, setCreatingList] = useState(false);
  const [listName, setListName] = useState("");
  const [lists, setLists] = useState([]);
  const [boardImage, setBoardImage] = useState();
  const [isError, setIsError] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  const apiKey = "84d36a91628ce69735b6b540af5062d2";
  const apiToken =
    "ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8";

  useEffect(() => {
    getLists();
    getBoardImage();
  }, []);

  function getLists() {
    axios
      .get(
        `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setLists(response.data);
        setIsLoaded(true);
      })
      .catch((error) => {
        setIsError(true);
        /* console.error("Error fetching data:", error); */
      });
  }

  function getBoardImage() {
    axios
      .get(
        `https://api.trello.com/1/boards/${id}?key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((res) => {
        if (res.data.prefs.backgroundImageScaled != null) {
          let url = "url(" + res.data.prefs.backgroundImageScaled[6].url + ")";
          setBoardImage(url);
        } else {
          setBoardImage(res.data.prefs.backgroundColor);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }

  const handleCreateList = () => {
    setCreatingList(true);
  };

  const handleAddList = () => {
    if (listName !== "") {
      axios
        .post(
          `https://api.trello.com/1/lists?name=${listName}&idBoard=${id}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          console.log("List created:");
          setLists([...lists, response.data]);
          setListName("");
          setCreatingList(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log(listName);
  };

  const handleCancel = () => {
    setListName("");
    setCreatingList(false);
  };

  const handleDeleteClick = (id) => {
    axios
      .put(
        `https://api.trello.com/1/lists/${id}/?closed=true&&key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        const updatedItems = lists.filter((item) => item.id !== id);
        setLists(updatedItems);
      })
      .catch((error) => {
        console.error("Error updating data:", error);
      });
  };

  if (isLoaded && !isError) {
    return (
      <Box padding={"30px"} h={"100vh"} background={boardImage}>
        <div className="board-lists">
          {lists.map((list) => (
            <List
              key={list.id}
              borderRadius={"5px"}
              h={"fit-content"}
              w={"250px"}
              bgColor={"#f0c3b1"}
              color="#393240"
              /* display="flex"
              justifyContent={"space-between"} */
              padding={"10px"}
            >
              <ListItem display={"flex"} justifyContent={"space-between"}>
                <Text
                  fontSize={"large"}
                  fontWeight={"bold"} /* mt={2} */ /* ml={2} */
                >
                  {list.name}
                </Text>
                <Button
                  size={"xs"}
                  bgColor={"#f0c3b1"}
                  onClick={() => handleDeleteClick(list.id)}
                  color="#393240"
                >
                  X
                </Button>
              </ListItem>
              <ListItem>
                <ListCards listId={list.id} />
              </ListItem>
            </List>
          ))}
          {creatingList ? (
            <Flex
              bgColor={"lavender"}
              height={"100px"}
              w={"250px"}
              borderRadius={"8px"}
            >
              <FormControl padding={"8px"}>
                <FormLabel htmlFor="item-name">List Name</FormLabel>
                <Stack direction="row" spacing={2}>
                  <Input
                    colorScheme="gray"
                    variant={"filled"}
                    id="item-name"
                    placeholder="Enter List name"
                    value={listName}
                    onChange={(e) => setListName(e.target.value)}
                  />
                  <Button
                    size={"sm"}
                    fontSize={"xs"}
                    colorScheme="blue"
                    onClick={handleAddList}
                  >
                    Add
                  </Button>
                  <Button
                    size={"sm"}
                    fontSize={"xs"}
                    colorScheme="red"
                    onClick={handleCancel}
                  >
                    Cancel
                  </Button>
                </Stack>
              </FormControl>
            </Flex>
          ) : (
            <Button
              mt={7}
              w={"150px"}
              colorScheme="purple"
              onClick={handleCreateList}
            >
              Create List
            </Button>
          )}
        </div>
      </Box>
    );
  } else {
    return (
      <>
        {isError ? (
          <Text>Error</Text>
        ) : (
          <Flex justifyContent={"center"} mt={"15rem"}>
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="pink"
              size="xl"
            />
          </Flex>
        )}
      </>
    );
  }
}

export default Board;
