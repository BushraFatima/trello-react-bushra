import React from "react";
import {ImHome} from "react-icons/im";
import './Header.css'
import { NavLink } from "react-router-dom";


export default function Header () {
  return (
    <div className="header" style={{backgroundColor:'#dbc9e0', color:"#594e57"}}>
      <i class="fa-brands fa-trello"/>
      <h1>Trello</h1>
      <NavLink to="/"><ImHome/></NavLink>
    </div>
  )
}

