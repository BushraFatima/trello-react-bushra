import {
  Text,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Button,
  useDisclosure,
  Flex,
} from "@chakra-ui/react";
import axios from "axios";
import CardInfo from "../CardInfo/CardInfo";

function CardModal({ card, cards, setCards }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  function deleteCard(id) {
    axios
      .delete(
        `https://api.trello.com/1/cards/${card.id}?key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((response) => {
        const updatedCards = cards.filter((card) => card.id !== id);
        setCards(updatedCards);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <>
      <Flex mt={"0.6rem"}>
        <Flex
          fontSize={"sm"}
          borderRadius={"5px"}
          bg={"#e5e1e8"}
          justifyContent={"space-between"}
          /* alignItems={"center"} */ width={"100%"}
        >
          <Button
            bg={"#e5e1e8"}
            onClick={onOpen}
            flexBasis={"100%"}
            color={"#818B95"}
            justifyContent={"flex-start"}
          >
            <Text color={"#393240"}>{card.name}</Text>
          </Button>
          <Button
            color={"#393240"}
            fontSize={"sm"}
            onClick={() => deleteCard(card.id)}
            background={"none"}
          >
            X
          </Button>
        </Flex>
      </Flex>

      <Modal isOpen={isOpen} onClose={onClose} size={"3xl"}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{card.name}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <CardInfo card={card} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}

export default CardModal;
