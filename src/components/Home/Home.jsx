import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import FocusLock from "react-focus-lock";
import {
  Heading,
  Box,
  Card,
  CardBody,
  Text,
  Grid,
  Button,
  ButtonGroup,
  Stack,
  Portal,
  PopoverBody,
  Input,
  FormControl,
  FormLabel,
  Popover,
  PopoverCloseButton,
  PopoverContent,
  PopoverArrow,
  PopoverTrigger,
} from "@chakra-ui/react";

import "./Home.css";
import axios from "axios";

export function Home({ userData }) {
  const [nameList, setNameList] = useState([]);

  // 1. Create a text input component

  const TextInput = React.forwardRef((props, ref) => {
    return (
      <FormControl>
        <FormLabel htmlFor={props.id}>{props.label}</FormLabel>
        <Input ref={ref} id={props.id} {...props} />
      </FormControl>
    );
  });

  function handleClick(name) {
    const url = "https://api.trello.com/1/boards";
    axios
      .post(
        `${url}?name=${name}&key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((res) => {
        setNameList([...nameList, res.data]);
      })
      .catch((err) => console.log(err));
  }

  // 2. Create the form
  const Form = ({ firstFieldRef, onCancel, onSave }) => {
    const [name, setName] = useState("");
    const [disabled, setDisable] = useState(true);

    function getName(e) {
      setName(e.target.value);
      if (e.target.value === "") {
        setDisable(true);
      } else {
        setDisable(false);
      }
    }

    return (
      <Stack spacing={4}>
        <TextInput
          label="Enter board name"
          id="board-name"
          ref={firstFieldRef}
          onChange={getName}
        />
        <ButtonGroup d="flex" justifyContent="flex-end">
          <Button variant="outline" onClick={onCancel}>
            Cancel
          </Button>
          <Button
            isDisabled={disabled}
            bgColor="pink"
            onClick={() => {
              onSave();
              handleClick(name);
            }}
          >
            Create
          </Button>
        </ButtonGroup>
      </Stack>
    );
  };

  // 3. Create the Popover
  // Ensure you set `closeOnBlur` prop to false so it doesn't close on outside click
  const PopoverForm = () => {
    const [isOpen, setIsOpen] = useState(false);
    const firstFieldRef = React.useRef(null);
    const open = () => setIsOpen(true);
    const close = () => setIsOpen(false);
    return (
      <>
        <Popover
          isOpen={isOpen}
          initialFocusRef={firstFieldRef}
          onOpen={open}
          onClose={close}
          placement="right"
          closeOnBlur={false}
        >
          <PopoverTrigger>
            <Button size="sm" mb="3px" ml="15px" bgColor="pink" color="#564860">
              Create Board{" "}
            </Button>
          </PopoverTrigger>
          <PopoverContent zIndex={4} p={5}>
            <FocusLock returnFocus persistentFocus={false}>
              <PopoverArrow bgColor="white" />
              <PopoverCloseButton />
              <Form
                firstFieldRef={firstFieldRef}
                onCancel={close}
                onSave={close}
              />
            </FocusLock>
          </PopoverContent>
        </Popover>
      </>
    );
  };

  function CreateBoard({ bname }) {
    return (
      <NavLink to={`/boards/${bname.id}`}>
        <Card key={bname.id} id={bname.id} height={"150px"} width={"250px"}>
          <CardBody
            color={"white"}
            _hover={{ opacity: "0.8" }}
            bgColor={"pink"}
            borderRadius={"5px"}
          >
            <Text fontSize={"18px"} fontWeight={"bold"}>
              {bname.name}
            </Text>
          </CardBody>
        </Card>
      </NavLink>
    );
  }

  return (
    <div className="home">
      <Heading width={"100%"} height={"7vh"} backgroundColor={"#ffdef0"}>
        <PopoverForm />
      </Heading>

      <Grid ml="80px" mt="50px" gap="2rem" templateColumns="repeat(4, 255px)">
        {userData &&
          userData.map((data) => (
            <>
              {
                <NavLink to={`/boards/${data.id}`}>
                  <Card id={data.id} key={data.id} h={"150px"} w={"250px"}>
                    <CardBody
                      color={"white"}
                      _hover={{ opacity: "0.8" }}
                      backgroundImage={
                        data.prefs.backgroundImage
                          ? data.prefs.backgroundImage
                          : "none"
                      }
                      bgColor={data.prefs.backgroundColor}
                      backgroundSize={"250px 150px"}
                      backgroundRepeat={"no-repeat"}
                      borderRadius={"5px"}
                    >
                      <Text fontSize={"18px"} fontWeight={"bold"}>
                        {data.name}
                      </Text>
                    </CardBody>
                  </Card>
                </NavLink>
              }
            </>
          ))}
        {nameList && nameList.map((name) => <CreateBoard bname={name} />)}
      </Grid>
    </div>
  );
}
