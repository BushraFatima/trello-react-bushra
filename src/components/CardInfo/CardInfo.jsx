import {
  Text,
  Flex,
  Editable,
  EditablePreview,
  EditableInput,
  Spinner
} from "@chakra-ui/react";
import { AddIcon } from "@chakra-ui/icons";
import axios from "axios";
import { useEffect, useState } from "react";
import CheckList from "../CheckList/CheckList";

function CardInfo({ card }) {
  const [checklists, setChecklists] = useState([]);
  const [isChecklistLoaded, setIsChecklistLoaded] = useState(false);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/cards/${card.id}/checklists?key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((response) => {
        setChecklists(response.data);
        setIsChecklistLoaded(true);
      })
      .catch((error) => {
       /*  console.log(error); */
       setIsError(true);
      });
  }, []);

  function createCheckList(event) {
    if (event === "") {
      return;
    }
    axios
      .post(
        `https://api.trello.com/1/checklists?name=${event}&idCard=${card.id}&key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((response) => {
        console.log(response);
        setChecklists([...checklists, response.data]);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  if (isChecklistLoaded && !isError) {
    return (
      <Flex justify={"space-between"} paddingBottom={"2rem"}>
        <Flex direction={"column"} gap={"2rem"}>
          {checklists.map((checklist) => {
            return (
              <CheckList
                checklist={checklist}
                checklists={checklists}
                setChecklists={setChecklists}
              ></CheckList>
            );
          })}
        </Flex>

        <Flex direction={"column"} pl={"10px"} h={"60px"}>
          <Text m={"0%"}>Add to card</Text>
          <Flex
            w={"150px"}
            justifyItems={"flex-end"}
            bgColor={"pink.200"}
            borderRadius={"5px"}
            padding={"3px"}
          >
            <AddIcon m={"9px"} fontSize={"xs"} />
            <Editable
              onSubmit={createCheckList}
              placeholder={"Add checklist"}
              defaultValue=""
            >
              <EditablePreview cursor={"pointer"} />
              <EditableInput />
            </Editable>
          </Flex>
        </Flex>
      </Flex>
    );
  } else {
      return (
        <>
        {isError ? 
        <Text>Error</Text>
        :
        <Flex justifyContent={'center'} mb={'3rem'}>
        <Spinner
          thickness='4px'
          speed='0.65s'
          emptyColor='gray.200'
          color='pink'
          size='lg'
        />
        </Flex> 
        }
        </>
      )
  }
}
export default CardInfo;
