import React, { useState, useEffect } from "react";
import { useDisclosure } from "@chakra-ui/react";
import {
  Box,
  Button,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Spinner,
  Text,
} from "@chakra-ui/react";
import axios from "axios";
import CardModal from "../CardModal/CardModal";

function ListCards({ listId }) {
  const [creatingCard, setCreatingCard] = useState(false);
  const [cardName, setCardName] = useState("");
  const [cards, setCards] = useState([]);
  const [isError, setIsError] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  const apiKey = "84d36a91628ce69735b6b540af5062d2";
  const apiToken =
    "ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8";

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setCards(response.data);
        setIsLoaded(true);
      })
      .catch((error) => {
        /* console.error("Error fetching data:", error); */
        setIsError(true);
      });
  }, []);

  const handleCreateCard = () => {
    setCreatingCard(true);
  };

  const handleAddCard = () => {
    if (cardName !== "") {
      axios
        .post(
          `https://api.trello.com/1/cards?name=${cardName}&idList=${listId}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          console.log("CheckItem created:");
          setCards([...cards, response.data]);
          setCardName("");
          setCreatingCard(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log(cardName);
  };

  const handleCancel = () => {
    setCardName("");
    setCreatingCard(false);
  };

  if (isLoaded && !isError) {
    return (
      <Box>
        <Flex direction={"column"}>
          {cards.map((card) => (
            <CardModal
              card={card}
              cards={cards}
              setCards={setCards}
            ></CardModal>
          ))}
        </Flex>
        {creatingCard ? (
          <Flex
            mt={"15px"}
            bgColor={"lavender"}
            padding={"5px"}
            borderRadius={"5px"}
          >
            <FormControl>
              <FormLabel htmlFor="card-name">Card Name</FormLabel>
              <Stack direction="row" spacing={2}>
                <Input
                  colorScheme="gray"
                  variant={"filled"}
                  id="card-name"
                  placeholder="Enter card name"
                  value={cardName}
                  onChange={(e) => setCardName(e.target.value)}
                />

                <Button
                  size={"sm"}
                  fontSize={"xs"}
                  colorScheme="red"
                  onClick={handleCancel}
                >
                  Cancel
                </Button>
              </Stack>
              <Button
                size={"sm"}
                fontSize={"xs"}
                mt={"7px"}
                colorScheme="blue"
                onClick={handleAddCard}
              >
                Add
              </Button>
            </FormControl>
          </Flex>
        ) : (
          <Button
            mt={"15px"}
            size={"sm"}
            bgColor={"#f0c3b1"}
            border={"1px solid gray"}
            onClick={handleCreateCard}
          >
            Create Card
          </Button>
        )}
      </Box>
    );
  } else {
    return (
      <>
        {isError ? (
          <Text>Error</Text>
        ) : (
          <Flex justifyContent={"center"}>
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="teal"
              size="md"
            />
          </Flex>
        )}
      </>
    );
  }
}

export default ListCards;
