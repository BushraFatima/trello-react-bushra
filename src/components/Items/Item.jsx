import React, { useState, useEffect } from "react";
import {
  Box,
  Button,
  ButtonGroup,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Stack,
  Text,
  Spinner,
} from "@chakra-ui/react";
import axios from "axios";
function Item({ checkListId }) {
  const [creatingItem, setCreatingItem] = useState(false);
  const [itemName, setItemName] = useState("");
  const [items, setItems] = useState([]);
  const [isError, setIsError] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);

  const apiKey = "84d36a91628ce69735b6b540af5062d2";
  const apiToken =
    "ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8";

  useEffect(() => {
    axios
      .get(
        `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        setItems(response.data);
        setIsLoaded(true);
      })
      .catch((error) => {
        /* console.error("Error fetching data:", error); */
        setIsError(true);
      });
  }, []);

  const handleCreateItem = () => {
    setCreatingItem(true);
  };

  const handleAddItem = () => {
    if (itemName !== "") {
      axios
        .post(
          `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${itemName}&key=${apiKey}&token=${apiToken}`
        )
        .then((response) => {
          console.log("CheckItem created:");
          setItems([...items, response.data]);
          setItemName("");
          setCreatingItem(false);
        })
        .catch((error) => {
          console.log(error);
        });
    }
    console.log(itemName);
  };

  const handleCancel = () => {
    setItemName("");
    setCreatingItem(false);
  };

  const handleDeleteClick = (id) => {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${checkListId}/checkItems/${id}?key=${apiKey}&token=${apiToken}`
      )
      .then((response) => {
        const updatedItems = items.filter((item) => item.id !== id);
        setItems(updatedItems);
      })
      .catch((error) => {
        console.error("Error updating data:", error);
      });
  };

  if (isLoaded && !isError) {
    return (
      <Box p={3}>
        <Flex direction={"column"} mt={"0%"} mb={"15px"}>
          {items.map((item) => (
            <Flex
              key={item.id}
              alignItems="center"
              justifyContent={"space-between"} /* marginTop="2" */
            >
              <Checkbox /* key={item.id} */ mt={2} /* isChecked */>
                {item.name}
              </Checkbox>
              <Button
                size={"xs"}
                onClick={() => handleDeleteClick(item.id)}
                variant="outline"
                colorScheme="red"
                mt={"5px"}
              >
                X
              </Button>
            </Flex>
          ))}
        </Flex>
        {creatingItem ? (
          <Flex>
            <FormControl>
              <FormLabel htmlFor="item-name">Item Name</FormLabel>
              <Stack direction="row" spacing={2}>
                <Input
                  id="item-name"
                  placeholder="Enter item name"
                  value={itemName}
                  onChange={(e) => setItemName(e.target.value)}
                />
                <ButtonGroup mt={"3px"}>
                  <Button
                    size={"sm"}
                    colorScheme="blue"
                    onClick={handleAddItem}
                  >
                    Add
                  </Button>
                  <Button size={"sm"} colorScheme="red" onClick={handleCancel}>
                    Cancel
                  </Button>
                </ButtonGroup>
              </Stack>
            </FormControl>
          </Flex>
        ) : (
          <Button size={"sm"} colorScheme="teal" onClick={handleCreateItem}>
            Create Item
          </Button>
        )}
      </Box>
    );
  } else {
    return (
      <>
        {isError ? (
          <Text>Error</Text>
        ) : (
          <Flex justifyContent={"center"}>
            <Spinner
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="teal"
              size="md"
            />
          </Flex>
        )}
      </>
    );
  }
}

export default Item;
