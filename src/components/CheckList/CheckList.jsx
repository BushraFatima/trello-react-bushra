import React, { useEffect, useState } from "react";
import "./CheckList.css";
import { Flex, Button, Text, Box } from "@chakra-ui/react";
import { CheckIcon } from "@chakra-ui/icons";
import Item from "../Items/Item";
import axios from "axios";

function CheckList({ checklist, checklists, setChecklists }) {
  function deleteCheckList(checklistID) {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${checklistID}?key=84d36a91628ce69735b6b540af5062d2&token=ATTAc2e211898dc5b4af7e6e42f50b9a45aadca31081c35648258031717387fe55da60627FB8`
      )
      .then((response) => {
        const updatedCheckLists = checklists.filter(
          (checklist) => checklist.id !== checklistID
        );
        setChecklists(updatedCheckLists);
      })
      .catch((error) => {
        console.error("Error updating data:", error);
      });
  }

  return (
    <Box w={"500px"} borderRadius={"8px"} bgColor={"aliceblue"}>
      <Flex
        key={checklist.id}
        className="checklist-header"
        align={"center"}
        justifyContent={"space-between"}
        padding={"10px"}
      >
        <Flex w={"60px"} justifyContent={"space-between"}>
          <CheckIcon mt={"3px"} mr={"7px"} />
          <Text fontSize={"16px"} fontWeight={"bold"}>
            {checklist.name}
          </Text>
        </Flex>
        <Button
          size={"sm"}
          border={"1px solid pink"}
          onClick={() => deleteCheckList(checklist.id)}
        >
          Delete
        </Button>
      </Flex>
      {<Item checkListId={checklist.id} />}
    </Box>
  );
}
export default CheckList;
